<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Domain\User\User;
use Carbon\Carbon;

/**
 * Class UsersTableSeeder
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 20; $i++) {
            $user = new User();
            $user->firstName = $i ? "test$i" : 'test';
            $user->lastName = $i ? "test$i" : 'test';
            $user->email = $i ? "test$i@test.com" : 'test@test.com';
            $user->password = Hash::make('testpass');
            $user->bio = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris et hendrerit metus, 
                            non viverra leo. Orci varius natoque penatibus et magnis dis parturient montes, nascetur 
                            ridiculus mus. Vivamus hendrerit neque eu eros pharetra facilisis. Fusce egestas neque vitae
                             ipsum egestas, at lobortis neque euismod. In mattis bibendum nulla.';
            $user->createdAt = Carbon::now();

            $user->save();
        }
    }
}
