<?php

namespace App\Domain\User;

use App\Domain\Core\Pagination;

/**
 * Interface UserRepositoryInterface
 * @package App\Domain\User
 */
interface UserRepositoryInterface
{
    /**
     * @param Pagination $pagination
     * @return mixed
     */
    public function all(Pagination $pagination);

    /**
     * @param int $id
     * @return mixed
     */
    public function byId(int $id);

    /**
     * @param User $user
     * @return mixed
     */
    public function store(User $user);

    /**
     * @param User $user
     * @return mixed
     */
    public function delete(User $user);
}