<?php

namespace App\Domain\User;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

/**
 * Class User
 * @package App\Domain\User
 *
 * @property integer $id
 * @property string $firstName
 * @property string $lastName
 * @property string $email
 * @property string $password
 * @property string $bio
 * @property Carbon $createdAt
 * @property Carbon $updatedAt
 */
class User extends Model
{
    use Eloquence, Mappable;

    /** @var string $table */
    protected $table = 'users';

    /** @var array $maps */
    public $maps = [
        'firstName' => 'first_name',
        'lastName' => 'last_name',
        'createdAt' => 'created_at',
        'updatedAt' => 'updated_at',
    ];

    /** @var array $casts */
    protected $casts = [
        'id' => 'integer',
        'updated_at' => 'datetime',
        'created_at' => 'datetime'
    ];
}