<?php

namespace App\Console\Commands;

use App\Domain\Core\Pagination;
use App\Domain\User\UserRepositoryInterface;
use Illuminate\Console\Command;

class TestGetData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test command to get data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param UserRepositoryInterface $userRepository
     *
     * @return mixed
     */
    public function handle(UserRepositoryInterface $userRepository)
    {
        $pagination = new Pagination();
        $users = $userRepository->all($pagination);

        print_r($users);
    }
}
