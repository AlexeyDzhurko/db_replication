<?php

namespace App\Console\Commands;

use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;
use Illuminate\Console\Command;

class TestStoreData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:store-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test command to store data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(UserRepositoryInterface $repository)
    {
        $user = User::create(
            'test',
            'test',
            'test@test.com',
            'lorem ipsum'
        );

        $repository->store($user);
    }
}
