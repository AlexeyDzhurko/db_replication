<?php

namespace App\Providers;

use App\Domain\User\UserRepositoryInterface;
use App\Infrastructure\Eloquent\Parent\ParentRepository;
use App\Infrastructure\Eloquent\Parent\ParentRepositoryInterface;
use App\Infrastructure\Eloquent\UserRepository;
use Illuminate\Support\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(UserRepositoryInterface::class, UserRepository::class);
    }
}
