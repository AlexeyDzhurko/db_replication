<?php

namespace App\Infrastructure\Eloquent;

use App\Domain\Core\Pagination;
use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class UserRepository
 * @package App
 */
class UserRepository implements UserRepositoryInterface
{
    /** @var User $model */
    private $model;

    /** @var Builder $builder */
    private $builder;

    /**
     * UserRepository constructor.
     * @param User $model
     */
    public function __construct(
        User $model
    ) {
        $this->model = $model;
    }

    /**
     * @param Pagination $pagination
     * @return LengthAwarePaginator|Builder[]|Collection|mixed
     */
    public function all(Pagination $pagination)
    {
        $this->builder = $this->model
            ->setConnection('slavepostgres')
            ->newQuery();

        if ($pagination) {
            return $this->builder->paginate($pagination->limit(), ['*'], 'page', $pagination->page());
        }

        return $this->builder->get();
    }

    /**
     * @param int $id
     * @return mixed|void
     */
    public function byId(int $id)
    {
        return $this->builder = $this->model
            ->setConnection('slavepostgres')
            ->newQuery()
            ->where('id', '=', $id)
            ->first();
    }

    /**
     * @param User $user
     * @return mixed|void
     */
    public function store(User $user)
    {
        $user->save();
    }

    /**
     * @param User $user
     * @return mixed|void
     * @throws \Exception
     */
    public function delete(User $user)
    {
        $user->delete();
    }
}