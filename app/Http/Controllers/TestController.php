<?php

namespace App\Http\Controllers;

use App\Domain\Core\Pagination;
use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

/**
 * Class TestController
 * @package App\Http\Controllers
 *
 * @property UserRepositoryInterface $userRepository
 */
class TestController extends Controller
{
    /** @var UserRepositoryInterface $userRepository */
    private $userRepository;

    /**
     * TestController constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     *
     */
    public function index()
    {
        $pagination = new Pagination();
        $users = $this->userRepository->all($pagination);

        dd($users);
    }

    /**
     * @param int $id
     */
    public function view(int $id)
    {
        $user = $this->userRepository->byId($id);

        dd($user);
    }

    public function store()
    {
        $rand = uniqid();

        $user = new User();
        $user->firstName = "testStore$rand";
        $user->lastName = "testStore$rand";
        $user->email = "test$rand@test.com";
        $user->password = Hash::make('testpass');
        $user->bio = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris et hendrerit metus, 
                            non viverra leo. Orci varius natoque penatibus et magnis dis parturient montes, nascetur 
                            ridiculus mus. Vivamus hendrerit neque eu eros pharetra facilisis. Fusce egestas neque vitae
                             ipsum egestas, at lobortis neque euismod. In mattis bibendum nulla.';
        $user->createdAt = Carbon::now();

        $this->userRepository->store($user);
    }
}
